import React from 'react';
import './App.css';

import UserList from './Components/UserList';
import LoginForm from './Components/LoginForm';
import Register from './Components/Register';
import Search from './Components/Search';


const App = () => {


  return (
    <div>
      <Register />
      <LoginForm />
      <UserList />
      <Search />

    </div>
  );
}

export default App;