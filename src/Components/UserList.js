import React, { useState, useEffect } from "react";
import axios from "axios";

function UserList() {
    const [userList, setUserList] = useState([]);

    useEffect(() => {
        // Gọi API để lấy danh sách người dùng
        axios.get("http://127.0.0.1:8000/api/QuanLyNguoiDung/DanhSach", {
            headers: {
                TokenNguyenVanLam: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
            },
        })
            .then(response => {
                setUserList(response.data);
            })
            .catch(error => {
                console.error("Error fetching user list:", error);
            });
    }, []);

    return (
        <div>
            <h1>User List</h1>
            <ul>
                {userList.map((user, index) => (
                    <li key={index}>
                        <strong>Username:</strong> {user.username}, <strong>Email:</strong> {user.email}
                        <strong>Password:</strong> {user.password}, <strong>Phone Number:</strong> {user.phone_number}
                        <strong>Email:</strong> {user.email}, <strong>Full Name:</strong> {user.full_name}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default UserList;
