import axios from 'axios';
import React, { useState } from 'react'

const Search = () => {
    const [query, setQuery] = useState('');
    const [users, setUsers] = useState([]);
    const [error, setError] = useState('');


    const searchUsers = async () => {
        const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c'
        try {
            const response = await axios.get(`http://127.0.0.1:8000/api/QuanLyNguoiDung/TimKiem?query=${query}`, {
                headers: {
                    TokenNguyenVanLam: `${token}`
                }
            });
            setUsers(response.data);
            setError('');

        } catch (err) {
            setError('An error occurred while fetching users')
            setUsers([]);
        }
    }

    return (
        <div>
            <h1>
                Search
            </h1>
            <input type="text"
                value={query}
                onChange={(e) => setQuery(e.target.value)}
                placeholder='Enter query' />

            <button onClick={searchUsers}>Search</button>
            {error && <p>{error}</p>}

            <ul>
                {users.map((user, index) => (
                    <li key={index}>
                        <p>Username: {user.username}</p>
                        <p>Password: {user.password}</p>
                        <p>Email: {user.email}</p>
                        <p>Phone: {user.phone_number}</p>
                        <p>Full Name: {user.full_name}</p>
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default Search