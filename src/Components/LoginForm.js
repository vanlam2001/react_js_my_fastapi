import axios from 'axios'
import React from 'react'
import { useState } from 'react'

const LoginForm = () => {
    const [username, setUserName] = useState("")
    const [password, setPassword] = useState("")
    const [errorMessage, setErrorMessage] = useState("")

    const handleLogin = async () => {
        try {
            const response = await axios.post(
                'http://127.0.0.1:8000/api/Dang-nhap',
                {
                    username,
                    passwords: password,
                },
                {
                    headers: {
                        Token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
                    },
                }
            );

            if (response.data.message === 'Đăng nhập thành công') {
                // Xử lý logic sau khi đăng nhập thành công
                console.log('Đăng nhập thành công');
            }
        } catch (error) {
            setErrorMessage('Tên đăng nhập hoặc mật khẩu không đúng');
        }
    };
    return (
        <div>
            <h2>Login From</h2>
            <div>
                <label htmlFor="username">Tên đăng nhập:</label>
                <input type="text" id="username" value={username} onChange={(e) => setUserName(e.target.value)} />
            </div>
            <div>
                <label htmlFor="password">Mật Khẩu</label>
                <input type="text" id='password' value={password} onChange={(e) => setPassword(e.target.value)} />
            </div>

            <button onClick={handleLogin}>Đăng nhập</button>
            {errorMessage && <p style={{ color: 'red' }}>{errorMessage}</p>}

        </div>
    )
}

export default LoginForm